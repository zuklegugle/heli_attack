extends KinematicBody2D

export(NodePath) onready var state_machine = get_node(state_machine) as SMStateMachine

var waypoints_enabled = true

func _ready():
	state_machine.set_blackboard_entry("position",global_position)

func _input(event):
	if event is InputEventMouseButton:
		if waypoints_enabled:
			state_machine.set_blackboard_entry("waypoint_position",get_global_mouse_position())

func _process(delta):
	waypoints_enabled = state_machine.get_blackboard_entry("waypoints_enabled") # update

func _physics_process(delta):
	state_machine.set_blackboard_entry("position",global_position) #update

extends Node

const DEFAULT_MASS = 2.0
const DEFAULT_MAX_SPEED = 400.0
const DEFAULT_SLOW_RADIUS = 200.0

const DEFAULT_ACCELERATION = 400.0

static func seek(
	velocity: Vector2,
	global_position: Vector2,
	target_position: Vector2,
	max_speed:= DEFAULT_MAX_SPEED,
	mass:= DEFAULT_MASS,
	acceleration:= DEFAULT_ACCELERATION
	) -> Vector2:
	velocity += (target_position - global_position).normalized() * (acceleration / mass)
	#var desired_velocity: Vector2 = (target_position - global_position).normalized() * max_speed
	#var steering: = (desired_velocity - velocity)
	return velocity

static func arrive_and_seek(
	velocity: Vector2,
	global_position: Vector2,
	target_position: Vector2,
	max_speed:= DEFAULT_MAX_SPEED,
	mass:= DEFAULT_MASS,
	acceleration:= DEFAULT_ACCELERATION,
	slow_radius:= DEFAULT_SLOW_RADIUS
	) -> Vector2:
	var to_target: = global_position.distance_to(target_position)
	velocity += (target_position - global_position).normalized() * (acceleration / mass)
	if to_target < slow_radius:
		var slow_vector = velocity * .05
		velocity = (velocity - slow_vector)
	#var desired_velocity: Vector2 = (target_position - global_position).normalized() * max_speed
	#var steering: = (desired_velocity - velocity)
	velocity = velocity.clamped(max_speed)
	return velocity

static func follow(
	velocity: Vector2,
	global_position: Vector2,
	target_position: Vector2,
	max_speed:= DEFAULT_MAX_SPEED,
	mass:= DEFAULT_MASS
	) -> Vector2:
	var desired_velocity: = (target_position - global_position).normalized() * max_speed
	var steering: = (desired_velocity - velocity) / mass
	return velocity + steering

static func arrive_to(
	velocity: Vector2,
	global_position: Vector2,
	target_position: Vector2,
	max_speed:= DEFAULT_MAX_SPEED,
	slow_radius:= DEFAULT_SLOW_RADIUS,
	mass:= DEFAULT_MASS
	) -> Vector2:
	var to_target: = global_position.distance_to(target_position)
	var desired_velocity: = (target_position - global_position).normalized() * max_speed
	if to_target < slow_radius:
		desired_velocity *= (to_target / slow_radius) * 0.8 + 0.2
	var steering: = (desired_velocity - velocity) / mass
	return velocity + steering

static func sb_thrust_toward(
	position : Vector2,
	target_position : Vector2,
	velocity : Vector2,
	thrust_force : float
	):
	var direction_to_target : Vector2 = (target_position - position).normalized()
	var desired_velocity = direction_to_target * thrust_force
	var steering = (desired_velocity - velocity) / 100
	return velocity + steering


static func sb_seek(
	position : Vector2, 
	target_position : Vector2,
	velocity : Vector2,
	thrust : float,
	thrust_force : float,
	max_thrust_force: float,
	max_steering_force : float
	):
	thrust += thrust_force
	thrust = clamp(thrust, 0, 1)
	var direction_to_target : Vector2 = (target_position - position)
	var desired_velocity : Vector2 = (direction_to_target.normalized() * max_thrust_force) * thrust
	var steering : Vector2 = (desired_velocity - velocity).normalized() * max_steering_force
	return steering
	 

extends Node

class_name AbleToDie

export(NodePath) onready var health = get_node_or_null(health)
export(bool) var immortal = false

signal died()

func kill():
	if !immortal:
		emit_signal("died")

func _ready():
	if health:
		health = health as Health
		health.connect("health_zero", self, "_on_zero_health")

func _on_zero_health(node):
	kill()

func _exit_tree():
	if health:
		health = health as Health
		health.disconnect("health_zero", self, "_on_zero_health")

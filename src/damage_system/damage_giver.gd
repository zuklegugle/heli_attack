extends Node

class_name DamageGiver

export(float) var damage = 0.0
export(bool) var debug = false

func apply_damage(damage_taker = null, amount : float = damage):
	if damage_taker:
		damage_taker.apply_damage(amount)
		if debug:
			print("DamageGiver: damage applied (", amount ,")")
	else:
		if debug:
			print("DamageGiver: Couldn't apply damage: no damage taker found")

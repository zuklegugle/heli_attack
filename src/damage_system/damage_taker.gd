extends Node

class_name DamageTaker

export(NodePath) onready var health = get_node_or_null(health)
export(bool) var enable = true

signal damage_applied(node, amount)

func apply_damage(amount : float):
	if enable:
		if health:
			health = health as Health
			var h_previous = health.health
			health.subtract(amount)
			emit_signal("damage_applied", self, clamp(h_previous - amount, 0, health.max_health))

func _on_hit(node):
#	if enable:
#		if node is Area2D:
#			for child in node.get_children():
#				if child is DamageGiver:
#					var dmg_giver = child as DamageGiver
#					dmg_giver.apply_damage(self)
#		elif node is PhysicsBody2D:
	pass

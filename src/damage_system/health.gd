extends Node

class_name Health

export(float) var max_health setget _set_max_health, _get_max_health
export(float) var health  setget _set_health, _get_health
export(bool) var debug = false

signal health_changed(node, from, to)
signal health_zero(node)
signal health_full(node)

func add(amount : float) -> float:
	var h_previous = health
	health = clamp(health + amount, 0, max_health)
	emit_signal("health_changed", self, h_previous, health)
	if health >= max_health:
		emit_signal("health_full", self)
	return health
func subtract(amount : float) -> float:
	var h_previous = health
	health -= amount
	health = clamp(health, 0, max_health)
	emit_signal("health_changed", self, h_previous, health)
	if health <= 0:
		if debug:
			print("health zero")
		emit_signal("health_zero", self)
	if debug:
		print("health removed")
	return health
func percentage() -> float:
	return (health / max_health) * 100
# setters & getters
func _set_health(amount):
	var h_previous = health
	health = amount
	health = clamp(health, 0, max_health)
	emit_signal("health_changed", self, h_previous, health)
	if health <= 0:
		emit_signal("health_zero", self)
func _get_health():
	return health
func _set_max_health(amount : float):
	max_health = amount
	max_health = clamp(max_health, 0, 99999)
func _get_max_health():
	return max_health

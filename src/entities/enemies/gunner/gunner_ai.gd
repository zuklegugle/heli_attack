extends Node

export(NodePath) onready var body = get_node_or_null(body)
export(NodePath) onready var hand_position = get_node(hand_position) as Position2D
export(NodePath) onready var weapon = get_node(weapon) as Weapon

var target : Node

func _ready():
	var players = get_tree().get_nodes_in_group("player")
	if players:
		target = players[0]
	
func _process(delta):
	if target:
		hand_position.look_at(target.global_position)
		weapon.shoot()

extends Line2D

class_name VectorLine

export(float) var line_scale

var origin : Vector2
var vector : Vector2 #vector to draw

onready var _length = vector.length()


func _ready():
	add_point(vector)
	add_point(vector * _length)

func _process(delta):
	set_point_position(0, origin)
	var endpoint = vector.normalized()
	set_point_position(1, vector * (vector.length() / line_scale)) 

extends KinematicBody2D

class_name Helicopter

export(NodePath) onready var ai_controller = get_node_or_null(ai_controller)

onready var _tail_rotor = $TailRotor
onready var sprite = $Sprite

onready var health = $Health
onready var healthbar = $ProgressBar
onready var effects = $Effects

func _ready():
	healthbar.value = health.health
	print(health.health)
	#if ai_controller:
	#	ai_controller.connect("state_changed",self,"_on_ai_state_changed")


func _process(delta):
	if ai_controller:
		if ai_controller.velocity.x > 0.1:
			rotation_degrees = lerp(rotation_degrees,10,delta * 3)
		elif ai_controller.velocity.x < -0.1:
			rotation_degrees = lerp(rotation_degrees,-10,delta * 3)
		else:
			rotation_degrees = 0.0


func _on_health_changed(node, amount_prev, amount):
	healthbar.value = health.health
	effects.play("blink")
	
func _on_death():
	queue_free()

extends Node2D

class_name HelicopterAgent

export(NodePath) onready var body = get_node(body) as KinematicBody2D

signal state_changed(state)

enum State {
	HOVERING,
	MOVING_TO_POINT
}

var max_speed: float = 300.0
var velocity: = Vector2.ZERO
var DISTANCE_THRESHOLD = 3.0
var slow_radius = 200.0
var current_state: int = State.HOVERING

var _sin_timer: = 0.0

func _physics_process(delta):
	_sin_timer += 0.1
	if _sin_timer > 360:
		_sin_timer = 0.0
	
	match(current_state):
		0: 
			_on_hover()
		1: 
			_on_moving_to_point()

func _on_hover():
	var target_global_position = get_global_mouse_position()
	if body.global_position.x != target_global_position.x and body.global_position.x > DISTANCE_THRESHOLD * 2:
		_change_state(1)
		return
	velocity = Vector2(velocity.x, cos(_sin_timer)*30)
	velocity = body.move_and_slide(velocity)
func _on_moving_to_point():
	var target_global_position = get_global_mouse_position()
	#if body.global_position.distance_to(target_global_position) < DISTANCE_THRESHOLD:
		#body.global_position.x = target_global_position.x
		#_change_state(State.HOVERING)
		#return
	velocity = SteeringBehaviours.arrive_and_seek(
		velocity,
		body.global_position,
		target_global_position,
		max_speed,
		400,
		1200,
		100
		)
	velocity = body.move_and_slide(velocity)

func _change_state(state: int):
	if state != current_state:
		current_state = state
		emit_signal("state_changed", current_state)
		#print("state changed to ",current_state)

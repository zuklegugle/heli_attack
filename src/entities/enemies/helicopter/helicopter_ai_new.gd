extends Node2D

export(NodePath) onready var body = get_node(body) as KinematicBody2D
export(Resource) var waypoint_list
export(NodePath) onready var vector_line = get_node(vector_line) as VectorLine
export(NodePath) onready var los_line = get_node(los_line) as VectorLine

var max_speed: float = 300.0
var velocity: = Vector2.ZERO
var DISTANCE_THRESHOLD = 3.0
var slow_radius = 200.0

var current_waypoint = null

var _last_los : Vector2

func _ready():
	pass

func _physics_process(delta):
	if !current_waypoint:
		current_waypoint = _select_random_waypoint()
	var target_global_position
	if current_waypoint:
		target_global_position = current_waypoint.global_position
	else:
		target_global_position = global_position
#	if body.global_position.distance_to(target_global_position) < DISTANCE_THRESHOLD:
#		current_waypoint = _select_random_waypoint()
	
	velocity = SteeringBehaviours.arrive_and_seek(
		velocity,
		body.global_position,
		target_global_position,
		max_speed,
		400,
		1200,
		100
		)
	
	var los: Vector2 = (current_waypoint.global_position - body.global_position)
	var los_delta = los - _last_los
	
	los_line.vector = los
	vector_line.vector = velocity
	
	velocity = body.move_and_slide(velocity)
	
	_last_los = los
	


func _select_random_waypoint():
	var list = waypoint_list as EnemyWaypointList
	var wp = list.get_random_waypoint()
	while(current_waypoint == wp):
		wp = list.get_random_waypoint()
	return wp

func on_waypoint_reached():
	current_waypoint = _select_random_waypoint()
	#print("waypoint reached")

func _on_Detector_area_entered(area):
	var wp = area.get_parent()
	if wp is EnemyWaypoint:
		if wp == current_waypoint:
			on_waypoint_reached()

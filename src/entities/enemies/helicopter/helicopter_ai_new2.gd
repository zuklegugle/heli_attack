extends Node2D

export(NodePath) onready var body = get_node(body) as KinematicBody2D
export(Resource) var waypoint_list

export(float) var max_speed = 300.0

var velocity: = Vector2.ZERO
var current_waypoint = null

# callbacks
func _physics_process(delta):
	if !current_waypoint:
		current_waypoint = _select_random_waypoint()
	var target_global_position
	if current_waypoint:
		target_global_position = current_waypoint.global_position
	else:
		target_global_position = global_position
	
	velocity = SteeringBehaviours.sb_thrust_toward(
		body.global_position,
		target_global_position,
		velocity,
		max_speed
		)
	
	velocity = body.move_and_slide(velocity)
# methods
func _select_random_waypoint():
	var list = waypoint_list as EnemyWaypointList
	var wp = list.get_random_waypoint()
	while(current_waypoint == wp):
		wp = list.get_random_waypoint()
	return wp

func on_waypoint_reached():
	current_waypoint = _select_random_waypoint()
	#print("waypoint reached")

func _on_Detector_area_entered(area):
	var wp = area.get_parent()
	if wp is EnemyWaypoint:
		if wp == current_waypoint:
			on_waypoint_reached()

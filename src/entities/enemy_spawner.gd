extends Node

class_name EnemySpawner

export(PackedScene) var enemy_type
export(NodePath) onready var spawn_position = get_node_or_null(spawn_position)

func spawn():
	if enemy_type:
		enemy_type = enemy_type as PackedScene
		var enemy = enemy_type.instance()
		if spawn_position:
			enemy.global_position = spawn_position.global_position
		else:
			enemy.global_position = Vector2.ZERO
			
		var scene = get_parent()
		var list = scene.get_node("Enemies")
		list.add_child(enemy)

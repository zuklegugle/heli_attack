extends Position2D

class_name EnemyWaypoint

export(Resource) var waypoint_list
export(float) var radius = 10.0 setget set_radius, get_radius
export(bool) var debug = false

onready var _shape = $Area2D/CollisionShape2D as CollisionShape2D

func _ready():
	if waypoint_list:
		var list = waypoint_list as EnemyWaypointList
		list.register(self)
		if debug:
			print("waypoint registered")

func _exit_tree():
	if waypoint_list:
		var list = waypoint_list as EnemyWaypointList
		list.unregister(self)
		if debug:
			print("waypoint unregistered")

func set_radius(value):
	radius = value
	if _shape:
		var sh = _shape.shape as CircleShape2D
		sh.radius = value

func get_radius():
	return radius

extends Resource

class_name EnemyWaypointList

var waypoints = []

func register(waypoint):
	if !waypoints.has(waypoint):
		waypoints.append(waypoint)

func unregister(waypoint):
	var wp_index = waypoints.find(waypoint)
	if wp_index > -1:
		waypoints.remove(wp_index)

func clear():
	waypoints.clear()

func get_random_waypoint():
	randomize()
	var chosen_wp_index = randi()%waypoints.size() + 0
	return waypoints[chosen_wp_index]

func get_random_waypoint_index():
	randomize()
	var chosen_wp_index = randi()%waypoints.size() + 0
	return chosen_wp_index

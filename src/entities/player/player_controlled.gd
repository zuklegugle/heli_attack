extends Controller

class_name PlayerController

export(NodePath) onready var body = get_node(body) as KinematicBody2D
export(NodePath) onready var hand_position = get_node(hand_position) as Position2D
export(NodePath) onready var weapon = get_node(weapon) as Weapon

export var speed := 400
export var jump_strength := 700.0
export var maximum_jumps := 2
export var double_jump_strength := 1200.0
export var gravity := 5000.0

const UP_DIRECTION := Vector2.UP

var holding_attack: bool = false

var _jumps_made := 0
var _velocity := Vector2.ZERO

#------INPUT----------------------
var input_enabled :bool = true
var horizontal_axis :float
var vertical_axis :float

var in_air :bool

var _is_falling :bool
var _is_jumping :bool
var _is_double_jumping :bool
var _is_jump_cancelled :bool
var _is_running :bool
var _is_idling :bool

func handle_input():
	if input_enabled:
		horizontal_axis = (
			Input.get_action_strength("move_right")
			- Input.get_action_strength("move_left")
		)
		vertical_axis = (
			Input.get_action_strength("ui_up")
			- Input.get_action_strength("ui_down")
		)
	else:
		horizontal_axis = 0.0
		vertical_axis = 0.0
	
	_is_jumping = Input.is_action_just_pressed("jump") and body.is_on_floor()
	_is_double_jumping = Input.is_action_just_pressed("jump") and _is_falling
	_is_jump_cancelled = Input.is_action_just_pressed("jump") and _velocity.y < 0.0

func platform_movement(delta):
	in_air = _velocity.y != 0.0 and not body.is_on_floor()
	_is_falling = _velocity.y > 0.0 and not body.is_on_floor()
	_is_idling = body.is_on_floor() and is_zero_approx(_velocity.x)
	_is_running = body.is_on_floor() and not is_zero_approx(_velocity.x)
	
	_velocity.x = horizontal_axis * speed
	_velocity.y += gravity * delta
	
	if _is_jumping:
		_jumps_made += 1
		_velocity.y = -jump_strength
	elif _is_double_jumping:
		_jumps_made += 1
		if _jumps_made <= maximum_jumps:
			_velocity.y = -double_jump_strength
	elif _is_jump_cancelled:
		#_velocity.y = 0.0
		pass
	elif _is_idling or _is_running:
		_jumps_made = 0
	
	_velocity = body.move_and_slide(_velocity,UP_DIRECTION)

func _process(delta):
	hand_position.look_at(get_global_mouse_position())
	if Input.is_mouse_button_pressed(BUTTON_LEFT):
		holding_attack = true
		weapon.shoot()
	else:
		holding_attack = false
func _physics_process(delta: float) -> void:
	#handle_input()
	#platform_movement(delta)
	pass

extends SMState

export(NodePath) onready var player_controller = get_node(player_controller) as PlayerController

func on_physics_process(delta):
	player_controller.handle_input()
	player_controller.platform_movement(delta)
	state_machine.parameter_set("running", player_controller._is_running)
	state_machine.parameter_set("in_air", player_controller.in_air)

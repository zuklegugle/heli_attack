extends Area2D

class_name Projectile

export var damage: float = 100.0
export var speed: float = 500.0

onready var dmg_giver = $DamageGiver

func _physics_process(delta):
	position += global_transform.x * speed * delta

func _on_Bullet_body_entered(body):
	queue_free()
	pass

func _on_bullet_area_entered(area):
		if area is Hitbox:
			var hitbox = area as Hitbox
			dmg_giver.apply_damage(hitbox.damage_taker, damage)
			queue_free()

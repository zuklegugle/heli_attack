extends Node

class_name Interpolate2D

export(NodePath) var canvas_item
export(NodePath) var follow_target

var target : Node2D
var update = false
var gt_prev : Transform2D
var gt_current : Transform2D

func _ready():
	canvas_item = get_node_or_null(canvas_item)
	target = get_node_or_null(follow_target)
	
	if canvas_item:
		canvas_item.set_as_toplevel(true)
		if !target:
			target = get_parent()
		canvas_item.global_transform = target.global_transform
	
	gt_prev = target.global_transform
	gt_current = target.global_transform
	
func update_transform():
	gt_prev = gt_current
	gt_current = target.global_transform

func _process(delta):
	if update:
		update_transform()
		update = false
	
	var f = clamp(Engine.get_physics_interpolation_fraction(), 0, 1)
	canvas_item.global_transform = gt_prev.interpolate_with(gt_current, f)

func _physics_process(delta):
	update = true

func get_transform(transform : Transform2D):
	var translation = transform.get_origin()
	return Transform2D(transform.get_rotation(), translation)

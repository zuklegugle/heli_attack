extends SMCondition

class_name CompareToBool

enum Flag {
	TRUE,
	FALSE
}

export(String) var entry_name: String
export(Flag) var condition

func check_condition(state_machine: SMStateMachine) -> bool:
	var entry = state_machine.parameter_get(entry_name)
	match(condition):
		Flag.TRUE:
			return entry == true
		Flag.FALSE:
			return entry == false
	return false

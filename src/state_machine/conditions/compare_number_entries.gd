extends SMCondition

enum Flag {
	GREATER_THAN,
	LESS_THAN,
	EQUAL
}

export(String) var entry_a: String
export(String) var entry_b: String
export(Flag) var comparision = Flag.GREATER_THAN

func check_condition(state_machine: SMStateMachine) -> bool:
	var e_a = state_machine.get_blackboard_entry(entry_a) as float
	var e_b = state_machine.get_blackboard_entry(entry_b) as float
	if e_a and e_b:
		match(comparision):
			Flag.GREATER_THAN: return e_a > e_b
			Flag.LESS_THAN: return e_a < e_b
			Flag.EQUAL: return e_a == e_b
		return false
	else:
		return false

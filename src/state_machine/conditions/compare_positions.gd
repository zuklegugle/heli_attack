extends SMCondition

export(String) var entry_a: String
export(String) var entry_b: String
export(float) var distance_threshold = 3.0
export(bool) var NOT = false

func check_condition(state_machine: SMStateMachine) -> bool:
	var position_a = state_machine.get_blackboard_entry(entry_a)
	var position_b = state_machine.get_blackboard_entry(entry_b)
	#print("pos_a: ",position_a," pos_b: ", position_b)
	if position_a and position_b:
		if !NOT:
			if position_a.distance_to(position_b) <= distance_threshold:
				return true
			else:
				return false
		else:
			if position_a.distance_to(position_b) >= distance_threshold:
				#print("pos_a: ",position_a," pos_b: ", position_b)
				return true
			else:
				#print("pos_a: ",position_a," pos_b: ", position_b)
				return false
	else:
		return false

extends SMCondition

class_name CompareToNumber

enum Flag {
	GREATER_THAN,
	LESS_THAN,
	EQUAL
}

export(String) var entry_name: String
export(float) var compare_to: float
export(Flag) var comparision = Flag.GREATER_THAN

func check_condition(state_machine: SMStateMachine) -> bool:
	var entry = state_machine.parameter_get(entry_name) as float
	if entry:
		match(comparision):
			Flag.GREATER_THAN: 
				return entry >= compare_to
			Flag.LESS_THAN: 
				return entry <= compare_to
			Flag.EQUAL:
				return entry == compare_to
		return false
	else:
		return false

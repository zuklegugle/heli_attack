extends Node

class_name SMState

var state_machine

signal state_changed(from,to)

func on_enter():
	pass

func on_process(delta):
	pass

func on_physics_process(delta):
	pass

func on_input(event):
	pass

func on_exit():
	pass

extends Node

class_name SMStateMachine

export(NodePath) onready var target = get_node(target) setget ,target_get
export(NodePath) onready var default_state = get_node(default_state) setget ,default_state_get

export(bool) var debug = false

onready var states = $States.get_children()
onready var transitions = $Transitions.get_children()

var current_state setget state_set,state_get

export(Dictionary) var parameters = {}

func change_state(new_state) -> bool:
	if new_state:
		if current_state:
			current_state.on_exit()
			current_state = new_state
			current_state.on_enter()
			if debug:
				print("state changed to ", current_state.name)
			return true
		else:
			current_state = new_state
			current_state.on_enter()
			if debug:
				print("state changed to ", current_state.name)
			return true
	else:
		if debug:
			print("change state failed", current_state.name)
		return false
func parameter_set(name: String, value):
	parameters[name] = value
	if debug:
		#print("entry [", name,"] added to blackboard")
		pass
func parameter_get(name: String):
	if name in parameters:
		return parameters[name]
	else:
		return null
#---callbacks
func _ready():
	for state in states:
		if state is SMState:
			state.state_machine = self #set this state_machine refrence for each state
	for transition in transitions:
		transition.state_machine = self #set this state_machine refrence for each transition
		transition.connect("state_transitioned",self,"_on_state_transitioned")
	
	if default_state:
		change_state(default_state) #go to default state
	else:
		print("WARNING: No default state selected")
func _process(delta):
	current_state.on_process(delta)
	for transition in transitions:
		transition.process_transition()
func _physics_process(delta):
	current_state.on_physics_process(delta)
func _input(event):
	current_state.on_input(event)
#--setters/getters--
func state_set(val):
	change_state(val)
func state_get():
	return current_state
func target_get():
	return target
func default_state_get():
	return default_state
func _on_state_transitioned(from, to):
	change_state(to)

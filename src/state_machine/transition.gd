extends Node

class_name SMTransition

export(NodePath) onready var from_state = get_node(from_state) as SMState
export(NodePath) onready var to_state = get_node(to_state) as SMState

var state_machine

export(Array, Resource) var conditions

signal state_transitioned(from, to)

func process_transition():
	if from_state == state_machine.current_state:
		var cond_num = conditions.size()
		var current_cond = 0
		for condition in conditions:
			if condition.check_condition(state_machine):
				current_cond += 1
			else:
				break
			if current_cond == cond_num:
				emit_signal("state_transitioned",from_state,to_state)

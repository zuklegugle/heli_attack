extends SMTransition

class_name SMTransitionCustom

func condition() -> bool:  # override this function
	return false

func _process(delta):
	if from_state == state_machine.state:
		if condition():
			emit_signal("state_transitioned",from_state,to_state)

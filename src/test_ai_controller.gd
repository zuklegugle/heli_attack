extends Node

export(NodePath) onready var body = get_node(body) as KinematicBody2D


var max_speed: float = 300.0
var velocity: = Vector2.ZERO
var DISTANCE_THRESHOLD = 3.0
var slow_radius = 200.0

var target_waypoint_position: Vector2

func move_to_point(target: Vector2) -> void:
	if body.global_position.distance_to(target) < DISTANCE_THRESHOLD:
		velocity = Vector2.ZERO
		return
	velocity = SteeringBehaviours.arrive_to(
		velocity,
		body.global_position,
		target,
		max_speed,
		slow_radius
		)
	velocity = body.move_and_slide(velocity)

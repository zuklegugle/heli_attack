extends Label

export(NodePath) var state_machine

func _ready():
	state_machine = get_node_or_null(state_machine)

func _process(delta):
	if state_machine:
		state_machine = state_machine as SMStateMachine
		text = "state: " + state_machine.current_state.name

extends Node2D

class_name SpriteFlipper

export(NodePath) onready var sprite = get_node(sprite) as Sprite

func _process(delta):
	var rot = abs(global_rotation_degrees)
	if (rot >= -90 and rot <= 90) or (rot >= 270 and rot <= 360):
		if sprite.flip_v:
			sprite.flip_v = false
	elif (rot >= 90 and rot <= 270):
		if !sprite.flip_v:
			sprite.flip_v = true

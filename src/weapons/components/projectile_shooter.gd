extends Node2D

class_name ProjectileShooter

export(PackedScene) onready var projectile
export(NodePath) onready var shooting_point = get_node(shooting_point) as Position2D

export var has_cooldown: bool = true
export var rate_of_fire: float = .5

signal on_cooldown
signal off_cooldown

onready var timer = $Timer
var _on_cooldown = false

func _ready():
	timer.wait_time = rate_of_fire

func shoot():
	if has_cooldown:
		if !_on_cooldown:
			var new_projectile = projectile.instance()
			new_projectile.global_transform = shooting_point.global_transform
			get_tree().get_root().add_child(new_projectile)
			emit_signal("on_cooldown")
			timer.start()
			_on_cooldown = true
		else:
			pass
	else:
		var new_projectile = projectile.instance()
		get_tree().get_root().add_child(new_projectile)
		new_projectile.transform = shooting_point.global_transform

func _on_Timer_timeout():
	_on_cooldown = false
	emit_signal("off_cooldown")

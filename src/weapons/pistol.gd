extends Weapon

class_name Pistol

export(NodePath) onready var _projectile_shooter = get_node(_projectile_shooter) as ProjectileShooter

func shoot():
	_projectile_shooter.shoot()
